import 'dart:convert';
import 'dart:math';

import 'package:flutter/services.dart';
import 'package:pointycastle/export.dart';
import 'package:pointycastle/pointycastle.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:walletconnect_flutter_v2/apis/sign_api/models/sign_client_models.dart';
import 'package:walletconnect_flutter_v2/apis/web3app/web3app.dart';
import 'package:walletconnect_flutter_v2/walletconnect_flutter_v2.dart';
import 'package:web3dart/crypto.dart';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart' as http;

class Service {
  String value = '';
  String publickey = '';

  ConnectResponse? resp;
  SessionData? session;
  Web3App? wcClient;

  Future Web3AppInstance() {
    return Web3App.createInstance(
        // relayUrl: 'wss://relay.walletconnect.com',
        projectId: '1958d35988b449e517442899fd5b9e9a',
        metadata: const PairingMetadata(
            name: 'Empoorio Riders',
            description: 'description',
            url: 'https://walletconnect.com',
            icons: ['assets/images/Riders Nuevo 512px.png']));
  }

  Future sign(Function onError) async {
    try {
      wcClient = await Web3AppInstance();

      resp = await wcClient!.connect(requiredNamespaces: {
        'eip155': const RequiredNamespace(chains: [
          'eip155:1'
        ], methods: [
          'personal_sign',
        ], events: [
          "connect",
          "disconnect"
        ])
      });

      Uri? uri = resp!.uri;

      print('RESPUESTA ======================================= ${uri.toString()}');

      launchUrlString(uri.toString());
      session = await resp!.session.future;

      print('==============================================${session.toString()}');

      value = session!.namespaces['eip155']!.accounts[0];
      List eip1551 = value.toString().split(':');

      return eip1551[2];
    } catch (e) {
      print(e);
      onError();
    }
  }


  String url = "http://35.174.138.211:9080";

  register(String publicKey) async {
    var address;
    var response;
    var res;
    var nonce;
    var resNonce;
    var s;
    // var privateKey;
    var sHex;

    try {
      address = EthereumAddress.fromHex(publicKey);
      print("PUBLIC KEY ============================================== $publicKey");
      print("DIRECCION HEX================================================ ${address.hex}");

      response = await http.post(Uri.parse('$url/signup'),
          body: jsonEncode({
            "address": publicKey,
            "username": 'user${Random().nextInt(999999)}',
            "nonce": Random().nextInt(999999),
          }),
          headers: {
            'Content-type': 'application/json',
          });

      res = jsonDecode(response.body);
      print(res.toString());

      if (response.statusCode == 200) {
        nonce = await http.get(Uri.parse('$url/$publicKey/nonce'));
        resNonce = jsonDecode(nonce.body);

        print(resNonce.toString());

        if (nonce.statusCode == 200) {
          var check = await http.get(Uri.parse('$url/$publicKey'));

          var resChek = jsonDecode(check.body);
          print(resChek);
          // signMessageWithMetaMask(web3, publicKey, resNonce['nonce']).then((value) {
          //   print(value);
          // });
          if (check.statusCode == 200) {
            var rng = Random.secure();
            EthPrivateKey priKey = EthPrivateKey.createRandom(rng);
            s = bytesToHex(priKey.privateKey);

            print("PRIVATE  ==================== $s");
            // //var sHex = EthereumAddress.fromHex(s);
            // print(s);
            // print(sHex);

            var privateKey =
                ECPrivateKey(BigInt.parse(s, radix: 16), ECDomainParameters('secp256k1'));

            print(privateKey.d);
            final signer = ECDSASigner(null, HMac(SHA256Digest(), 64));
            signer.init(true, PrivateKeyParameter<ECPrivateKey>(privateKey));
            // final messageBytes = utf8.encode('este es un mensaje');
            // print(messageBytes);
            // final messageDigest = SHA256Digest().process(Uint8List.fromList(messageBytes));
            // print(messageDigest);
            final nonceBytes = utf8.encode(resNonce['nonce'].toString());
            print(nonceBytes);
            final nonceDigest = SHA256Digest().process(Uint8List.fromList(nonceBytes));
            print(nonceDigest);
            final combinedDigest = SHA256Digest().process(Uint8List.fromList(nonceDigest));
            print(combinedDigest);
            final sig = signer.generateSignature(combinedDigest);

            final sigString = sig.toString().replaceAll('(', '').replaceAll(')', '');

            final sigList = sigString.split(',');

            //   // final r = sig[0].toString();
            //   // final s = sig[1].toString();
            print("SIGNATURE ===================================== 0x${sigList[0] + sigList[1]}");
          }
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
