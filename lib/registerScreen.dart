import 'package:flutter/material.dart';
import 'package:registe_example/service.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String address = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  Service().sign(() {}).then((value) {
                    setState(() {
                      address = value;
                    });
                  });
                },
                child: Text('Connect')),
            Text(address),
            if (address != '')
              ElevatedButton(
                  onPressed: () {
                    Service().register(address);
                  },
                  child: Text('register'))
          ],
        ),
      ),
    );
  }
}
